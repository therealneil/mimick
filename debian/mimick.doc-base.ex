Document: mimick
Title: Debian mimick Manual
Author: <insert document author here>
Abstract: This manual describes what mimick is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/mimick/mimick.sgml.gz

Format: postscript
Files: /usr/share/doc/mimick/mimick.ps.gz

Format: text
Files: /usr/share/doc/mimick/mimick.text.gz

Format: HTML
Index: /usr/share/doc/mimick/html/index.html
Files: /usr/share/doc/mimick/html/*.html
