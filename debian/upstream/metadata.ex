# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/mimick/issues
# Bug-Submit: https://github.com/<user>/mimick/issues/new
# Changelog: https://github.com/<user>/mimick/blob/master/CHANGES
# Documentation: https://github.com/<user>/mimick/wiki
# Repository-Browse: https://github.com/<user>/mimick
# Repository: https://github.com/<user>/mimick.git
